{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "zonal-instances.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common GCP labels
*/}}
{{- define "zonal-instances.gcpLabels" -}}
customer: {{ .Values.customerName }}
env: {{ .Values.environment }}
{{- end }}

{{/*
Common k8s labels
*/}}
{{- define "zonal-instances.k8sLabels" -}}
helm.sh/chart: {{ include "zonal-instances.chart" . }}
app.kubernetes.io/name: app1
app.kubernetes.io/customer: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Calculate provisioning model and automatic restart according to preemptible.
*/}}
{{- define "zonal-instances.provisioningModel" -}}
{{- if .Values.preemptible }}SPOT{{ else }}STANDARD{{ end }}
{{- end }}
{{- define "zonal-instances.automaticRestart" -}}
{{- if .Values.preemptible }}false{{ else }}true{{ end }}
{{- end }}

