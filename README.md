# blog
## Hugo - Static Site
### Usage
* Listen on `192.168.2.3` and serve `./content` using `hugo`.
* `-D` will generate pages marked as draft, too.
* Firefox container uses `firefox-accessible`.
```bash
docker run --rm \
  -it \
  --user $UID:$GID \
  --network firefox-accessible \
  -v $(pwd):/src \
  --workdir=/src \
  klakegg/hugo:0.101.0-ext-ubuntu-ci hugo server --baseURL 192.168.2.3 -D --disableFastRender
```
* Generate static files in `./public` without draft (`-D`). 
  * CI does the same for `Gitlab Pages`.
  * Uses image as per Hugo's [docs](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/)
```bash
docker run --rm \
  -it \
  --user $UID:$GID \
  -it \
  --user $UID:$GID \
  --network firefox-accessible \
  -v $(pwd):/src \
  --workdir=/src \
  klakegg/hugo:0.101.0-ext-ubuntu-ci hugo
```

### Container image
* [docs](https://hub.docker.com/r/klakegg/hugo)
#### ext tag
* `ext` has extended features installed.
```
Table of Hugo extention features and the version when images first support the feature:
Feature 	Alpine 	Debian 	Ubuntu
Hugo extended 	0.43 	0.43 	0.43
PostCSS 	0.56.0 	0.43 	0.43
NodeJS 	0.54.0 	0.54.0 	0.54.0
Yarn 	0.54.0 	0.54.0 	0.54.0
Git 	0.56.0 	0.56.0 	0.56.0
Autoprefixer 	0.57.0 	0.57.0 	0.57.0
Go 	0.68.0 	0.68.0 	0.68.0
Babel 	0.71.0 	0.71.0 	0.71.0
rst2html 	0.81.0 	0.81.0 	0.81.0
```
#### ci tag
The ci images are prepared for use in configuration for continuous integration/deployment.  
  
Difference between normal images and ci images:
* Environment variable HUGO_ENV: production
* Entrypoint is empty

### Tutorial
[quickstart](https://gohugo.io/getting-started/quick-start/).  
[hugo docker image](https://hub.docker.com/r/klakegg/hugo/).  
[hugo theme](https://github.com/rhazdon/hugo-theme-hello-friend-ng).  
Create site structure in new dir `quickstart`.
```bash
 docker run --rm \
  -it \
  --user $UID:$GID \
  -v $(pwd):/src \
  --workdir=/src \
  --user $(id -u):$(id -g) klakegg/hugo:0.101.0-ext-ubuntu-ci new site quickstart
```
Create some content for page `foo`.
```bash
echo '---
title: "Page title"
date: 2020-01-02T14:47:11+02:00
draft: true
---

# Best title
Hello, world!' > quickstart/content/foo.md
```
Start/Generate site locally including `draft` content with above command.

#### Styling images
Enable `unsafe` HTML rendering for your renderer.
```toml
    [markup.goldmark.renderer]
      hardWraps = false
      unsafe = true
      xhtml = false
```
Write HTML.
```html
<img class="center" src="/blog/20221104-crossplane/argocd-in-action-zoomed.png">
```
